--TEST--
test2() Basic test
--EXTENSIONS--
blurhash
--FILE--
<?php
$i = file_get_contents('./tests/test.jpg');
var_dump(bh_encode_blob(4,3,$i));
?>
--EXPECT--
string(28) "LWNmvQ?a_4oMo|Rja0t6-qoMIUNF"

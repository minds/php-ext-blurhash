/* blurhash extension for PHP */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h"
#include "php_blurhash.h"
#include "blurhash_arginfo.h"

#include "encode.h"
#include "encode.c"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <stdio.h>

/* For compatibility with older PHP versions */
#ifndef ZEND_PARSE_PARAMETERS_NONE
#define ZEND_PARSE_PARAMETERS_NONE() \
	ZEND_PARSE_PARAMETERS_START(0, 0) \
	ZEND_PARSE_PARAMETERS_END()
#endif

const char *blurHashForFile(int xComponents, int yComponents,const char *filename);

const char *blurHashForFile(int xComponents, int yComponents,const char *filename) {
	int width, height, channels;
	unsigned char *data = stbi_load(filename, &width, &height, &channels, 3);
	if(!data) return NULL;

	const char *hash = blurHashForPixels(xComponents, yComponents, width, height, data, width * 3);

	stbi_image_free(data);

	return hash;
}

const char *blurHashForBlob(int xComponents, int yComponents,const unsigned char *blob, size_t length);
const char *blurHashForBlob(int xComponents, int yComponents,const unsigned char *blob, size_t length) {
	int width, height, channels;
	unsigned char *data = stbi_load_from_memory(blob, length, &width, &height, &channels, 3);
	if(!data) return NULL;

	const char *hash = blurHashForPixels(xComponents, yComponents, width, height, data, width * 3);

	stbi_image_free(data);

	return hash;
}

/* {{{ string bh_encode( [ string $var ] ) */
PHP_FUNCTION(bh_encode)
{
	char *var = "";
	size_t var_len = sizeof("") - 1;
	zend_long xComponent = 4;
	zend_long yComponent = 3;

	ZEND_PARSE_PARAMETERS_START(3, 3)
		Z_PARAM_LONG(xComponent)
		Z_PARAM_LONG(yComponent)
		Z_PARAM_STRING(var, var_len)
	ZEND_PARSE_PARAMETERS_END();

	const char *hash = blurHashForFile(xComponent, yComponent, var);
	zend_string *retval = strpprintf(0, "%s", hash);

	RETURN_STR(retval);
}
/* }}}*/

/* {{{ string bh_encode( [ string $var ] ) */
PHP_FUNCTION(bh_encode_blob)
{
	char *var = "";
	size_t var_len = sizeof("") - 1;
	zend_long xComponent = 4;
	zend_long yComponent = 3;

	ZEND_PARSE_PARAMETERS_START(3, 3)
		Z_PARAM_LONG(xComponent)
		Z_PARAM_LONG(yComponent)
		Z_PARAM_STRING(var, var_len)
	ZEND_PARSE_PARAMETERS_END();

	const char *hash = blurHashForBlob(xComponent, yComponent, (unsigned char *) var, var_len);
	zend_string *retval = strpprintf(0, "%s", hash);

	RETURN_STR(retval);
}
/* }}}*/

/* {{{ PHP_RINIT_FUNCTION */
PHP_RINIT_FUNCTION(blurhash)
{
#if defined(ZTS) && defined(COMPILE_DL_BLURHASH)
	ZEND_TSRMLS_CACHE_UPDATE();
#endif

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION */
PHP_MINFO_FUNCTION(blurhash)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "blurhash support", "enabled");
	php_info_print_table_end();
}
/* }}} */

/* {{{ blurhash_module_entry */
zend_module_entry blurhash_module_entry = {
	STANDARD_MODULE_HEADER,
	"blurhash",					/* Extension name */
	ext_functions,					/* zend_function_entry */
	NULL,							/* PHP_MINIT - Module initialization */
	NULL,							/* PHP_MSHUTDOWN - Module shutdown */
	PHP_RINIT(blurhash),			/* PHP_RINIT - Request initialization */
	NULL,							/* PHP_RSHUTDOWN - Request shutdown */
	PHP_MINFO(blurhash),			/* PHP_MINFO - Module info */
	PHP_BLURHASH_VERSION,		/* Version */
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_BLURHASH
# ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
# endif
ZEND_GET_MODULE(blurhash)
#endif
